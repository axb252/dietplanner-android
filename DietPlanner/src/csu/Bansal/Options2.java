package csu.Bansal;

import csu.Bansal.R;
import android.app.Activity;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
public class Options2 extends Activity {

	//class variables
	
	//buttons, one for collecting the desired # of meals per day and the other to populate the schedule
	Button btnSelect;
	Button btnNext;
	
	//edit text area for meal # input
	EditText editText1;
	
	//limitation checkboxes
	CheckBox chkEggs;
	CheckBox chkGluten;
	CheckBox chkHindu;
	CheckBox chkHindu2;
	CheckBox chkJewish;
	CheckBox chkLactose;
	CheckBox chkMuslim;
	CheckBox chkNuts;
	CheckBox chkSeafood;
	CheckBox chkVegan;
	CheckBox chkVegetarian;
	
	//week checkboxes
	CheckBox chkMonday;
	CheckBox chkTuesday;
	CheckBox chkWednesday;
	CheckBox chkThursday;
	CheckBox chkFriday;
	CheckBox chkSaturday;
	CheckBox chkSunday;
	
	//the corresponding week boolean variables that will be passed to next intent
	boolean chkMondayB = false;
	boolean chkTuesdayB = false;
	boolean chkWednesdayB = false;
	boolean chkThursdayB = false;
	boolean chkFridayB = false;
	boolean chkSaturdayB = false;
	boolean chkSundayB = false;
	
	//the corresponding limitation boolean variables that will be passed to next intent
	boolean chkEggsB = false;
	boolean chkGlutenB = false;
	boolean chkHinduB = false;
	boolean chkHindu2B = false;
	boolean chkJewishB = false;
	boolean chkLactoseB = false;
	boolean chkMuslimB = false;
	boolean chkNutsB = false;
	boolean chkSeafoodB = false;
	boolean chkVeganB = false;
	boolean chkVegetarianB = false;
	
	//what is input as # meals per day
	int amount = 0;
   
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.options2);
		
		
		Intent intent = getIntent();		
		Bundle myBundle = intent.getExtras();
		
		//tie in xml with java
		chkEggs = (CheckBox) findViewById(R.id.chkEggs);
		chkGluten = (CheckBox) findViewById(R.id.chkGluten);
		chkHindu = (CheckBox) findViewById(R.id.chkHindu);
		chkHindu2 = (CheckBox) findViewById(R.id.chkHindu2);
		chkJewish = (CheckBox) findViewById(R.id.chkJewish);
		chkLactose = (CheckBox) findViewById(R.id.chkLactose);
		chkMuslim = (CheckBox) findViewById(R.id.chkMuslim);
		chkNuts = (CheckBox) findViewById(R.id.chkNuts);
		chkSeafood = (CheckBox) findViewById(R.id.chkSeafood);
		chkVegan = (CheckBox) findViewById(R.id.chkVegan);
		chkVegetarian = (CheckBox) findViewById(R.id.chkVegetarian);
		
		chkMonday = (CheckBox) findViewById(R.id.chkMonday);
		chkTuesday = (CheckBox) findViewById(R.id.chkTuesday);
		chkWednesday = (CheckBox) findViewById(R.id.chkWednesday);
		chkThursday = (CheckBox) findViewById(R.id.chkThursday);
		chkFriday = (CheckBox) findViewById(R.id.chkFriday);
		chkSaturday = (CheckBox) findViewById(R.id.chkSaturday);
		chkSunday = (CheckBox) findViewById(R.id.chkSunday);
		
		btnSelect = (Button) findViewById(R.id.btnSelect);
		btnNext = (Button) findViewById(R.id.btnNext);
		
		editText1 = (EditText) findViewById(R.id.editText1);
		
		//on click, set the integer value in the edittext area to the amount variable
		btnSelect.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				
				amount = Integer.parseInt(editText1.getText().toString());
			}
		});
		
		//on click, send all relevant information to next intent 
		btnNext.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				
				//boolean checks for the days of the week requested
				if(chkMonday.isChecked()){
					chkMondayB = true;
				}
				if(chkTuesday.isChecked()){
					chkTuesdayB = true;
				}
				if(chkWednesday.isChecked()){
					chkWednesdayB = true;
				}
				if(chkThursday.isChecked()){
					chkThursdayB = true;
				}
				if(chkFriday.isChecked()){
					chkFridayB = true;
				}
				if(chkSaturday.isChecked()){
					chkSaturdayB = true;
				}
				if(chkSunday.isChecked()){
					chkSundayB = true;
				}
				
				//boolean checks for dietary restrictions
				if(chkEggs.isChecked()){
					chkEggsB = true;
				}
				if(chkGluten.isChecked()){
					chkGlutenB = true;
				}
				if(chkHindu.isChecked()){
					chkHinduB = true;
				}
				if(chkHindu2.isChecked()){
					chkHindu2B = true;
				}
				if (chkJewish.isChecked()) {
					chkJewishB = true;
				}
				if (chkLactose.isChecked()) {
					chkLactoseB = true;
				}
				if (chkMuslim.isChecked()) {
					chkMuslimB = true;
				}
				if (chkNuts.isChecked()) {
					chkNutsB = true;
				}
				if (chkSeafood.isChecked()) {
					chkSeafoodB = true;
				}
				if (chkVegan.isChecked()) {
					chkVeganB = true;
				}
				if (chkVegetarian.isChecked()) {
					chkVegetarianB = true;
				}
				Intent intent;
				intent = new Intent(getApplicationContext(), Options3.class);
				Bundle myData = new Bundle();
				
				
				//creates the two boolean arrays to be passed over
				boolean[] days = {chkMondayB, chkTuesdayB, chkWednesdayB, chkThursdayB, chkFridayB, chkSaturdayB, chkSundayB};
				boolean[] choices = {chkEggsB, chkGlutenB, chkHinduB, chkHindu2B, chkJewishB, chkLactoseB, chkMuslimB,
									chkNutsB, chkSeafoodB, chkVeganB, chkVegetarianB};
				
				//set the bundle data
				myData.putBooleanArray("choices", choices);
				myData.putBooleanArray("days", days);
				//myData.putStringArray("items", items);
				myData.putInt("amount", amount);
				intent.putExtras(myData); 
				startActivity(intent);
				
				
			}
		});

	}

	
}
