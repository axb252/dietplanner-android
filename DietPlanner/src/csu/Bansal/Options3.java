package csu.Bansal;

import java.util.ArrayList;
import java.util.Random;

import csu.Bansal.R;

import android.app.Activity;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.widget.TextView;
import android.widget.Toast;

public class Options3 extends Activity {
	
	//using sqlite
	SQLiteDatabase db;

	//button to populate schedule 
	Button btnCheckDB;
	
	//textview that will display output
	TextView txtMsg;
	
	//path to database
	String myDbPath = "data/data/csu.Bansal/databases/foods.s3db";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.options3);
		
		//tie xml and java
		btnCheckDB = (Button) findViewById(R.id.btnCheckDB);
		txtMsg = (TextView) findViewById(R.id.txtMsg);
		Intent intent = getIntent();
		Bundle myBundle = intent.getExtras();

		//get relevant information from previous intent 
		final int amount = myBundle.getInt("amount");
		final boolean[] choices = myBundle.getBooleanArray("choices");
		final boolean[] days = myBundle.getBooleanArray("days");

		//open the database
		try {
			db = SQLiteDatabase.openDatabase(myDbPath, null,
					SQLiteDatabase.CREATE_IF_NECESSARY);
		} catch (SQLiteException e) {
		}

		btnCheckDB.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				try {
					//if the database isn't open, re-open it
					if (!db.isOpen()) {
						try {
							db = SQLiteDatabase.openDatabase(myDbPath, null,
									SQLiteDatabase.CREATE_IF_NECESSARY);

						} catch (SQLiteException e) {

						}

					}
					txtMsg.append("\n");
					
					//string array of the data that will be traversed through in the database
					String[] columns = { "Name", "Eggs", "Gluten_free",
							"Hindu_basic", "Hindu_extreme", "Kosher", "Dairy",
							"Halal", "Nuts", "GeoLocation", "Vegan",
							"Vegetarian", "Weight", "Beverage", "Breakfast" };

					//Cursor object for database traversal 
					Cursor c = db.query("Foods", columns, null, null, null,
							null, "Name");

					//sets all the relevant column indexes 
					int nameCol = c.getColumnIndex("Name");
					int eggsCol = c.getColumnIndex("Eggs");
					int glutenCol = c.getColumnIndex("Gluten_free");
					int hindu1Col = c.getColumnIndex("Hindu_basic");
					int hindu2Col = c.getColumnIndex("Hindu_extreme");
					int kosherCol = c.getColumnIndex("Kosher");
					int dairyCol = c.getColumnIndex("Dairy");
					int halalCol = c.getColumnIndex("Halal");
					int nutsCol = c.getColumnIndex("Nuts");
					int fishCol = c.getColumnIndex("GeoLocation");
					int veganCol = c.getColumnIndex("Vegan");
					int vegetCol = c.getColumnIndex("Vegetarian");
					int weightCol = c.getColumnIndex("Weight");
					int bevCol = c.getColumnIndex("Beverage");
					int breakCol = c.getColumnIndex("Breakfast");

					//declare separate food arrays; w1 is weightage of 1, w2 is weightage of 2, w3 is weightage of 3
					//a meal is weight 4, so depending on what is picked, you will have to add to 4
					ArrayList<String> foodsW1 = new ArrayList<String>();
					ArrayList<String> foodsW2 = new ArrayList<String>();
					ArrayList<String> foodsW3 = new ArrayList<String>();
					ArrayList<String> beverages = new ArrayList<String>();
					ArrayList<String> breakfast = new ArrayList<String>();

					//list that holds final meals
					ArrayList<String> finalList = new ArrayList<String>();
					

					// count through the database
					while (c.moveToNext()) {
						columns[0] = c.getString(nameCol);
						columns[1] = c.getString(eggsCol);
						columns[2] = c.getString(glutenCol);
						columns[3] = c.getString(hindu1Col);
						columns[4] = c.getString(hindu2Col);
						columns[5] = c.getString(kosherCol);
						columns[6] = c.getString(dairyCol);
						columns[7] = c.getString(halalCol);
						columns[8] = c.getString(nutsCol);
						columns[9] = c.getString(fishCol);
						columns[10] = c.getString(veganCol);
						columns[11] = c.getString(vegetCol);
						columns[12] = Integer.toString((c.getInt(weightCol)));
						columns[13] = c.getString(bevCol);
						columns[14] = c.getString(breakCol);

						// necessary so break doesn't break out of cursor
						// counting while loop
						while (true) {
							//this checks to make the food being checked is not on the restrictions list
							if (choices[0] == true && columns[1].equals("y"))// eggs
								break;
							else if (choices[1] == true
									&& columns[2].equals("n"))// gluten free
								break;
							else if (choices[2] == true
									&& columns[3].equals("n"))// hindu basic
								break;
							else if (choices[3] == true
									&& columns[4].equals("n"))// hindu extreme

								break;
							else if (choices[4] == true
									&& columns[5].equals("n"))// kosher
								break;
							else if (choices[5] == true
									&& columns[6].equals("y"))// dairy
								break;
							else if (choices[6] == true
									&& columns[7].equals("n")) // halal
								break;
							else if (choices[7] == true
									&& columns[8].equals("y")) // nuts
								break;
							else if (choices[8] == true
									&& columns[9].equals("Sea")) // fish
								break;
							else if (choices[9] == true
									&& columns[10].equals("n")) // vegan
								break;
							else if (choices[10] == true
									&& columns[11].equals("n")) // vegetarian
								break;
							// else add to the appropriate list
							else {
								
								//if it is a beverage, add to beverage list
								if (columns[13].equals("y")) {
									beverages.add(columns[0]);
								//if it is a breakfast food, as to breakfast list
								} else if (columns[14].equals("y")) {
									breakfast.add(columns[0]);
								//if it is weightage 1, add to W1 list
								} else if (c.getInt(weightCol) == 1) {
									foodsW1.add(columns[0]);
								//if it is weightage 2, add to W2 list	
								} else if (c.getInt(weightCol) == 2) {
									foodsW2.add(columns[0]);
								//if it is weightage 3, add to W3 list
								} else {
									foodsW3.add(columns[0]);
								}
							}
							break;
						}
					}

					//set random object and the counter to determine how many days were checked
					Random rand = new Random();
					int counter = 0;
					for (int i = 0; i < 7; i++) {
						if (days[i] == true) {
							counter++;
						}
					}
					Integer total = amount * counter; // count the total number
														// of meals to be made

					String food1 = "";
					String food2 = "";
					String food3 = "";
					String drink = "";

					// populate final list of foods
					for (int i = 1; i < total + 1; i++) {
						int value = rand.nextInt(4) + 1;
						
						if (value == 1) {// breakfast food
							int numberOfBreakfast = breakfast.size() - 1;
							int value2 = 0;
							int value3 = 0;
							int value4 = 0;
							//get 3 unique random numbers between 0 and the size of the breakfast list
							while ((value2 == value3) || (value2 == value4)
									|| (value3 == value4)) {
								value2 = (int) (Math.random()
										* (numberOfBreakfast - 0) + 0);
								value3 = (int) (Math.random()
										* (numberOfBreakfast - 0) + 0);
								value4 = (int) (Math.random()
										* (numberOfBreakfast - 0) + 0);
							}
							//set the food and drink variables to something in those lists
							food1 = breakfast.get(value2);
							food2 = breakfast.get(value3);
							food3 = breakfast.get(value4);

							drink = beverages.get(beverages
									.indexOf("orange_juice"));

							String temp = "\n To eat: \n 1.): " + food1
									+ " \n 2.): " + food2 + " \n 3.): " + food3
									+ " \n and to drink: " + drink + "\n";
							//set the final list
							finalList.add(temp);
							
						} else if (value == 2) {// low weight food
							int numberOf1Weight = foodsW1.size() - 1;
							int value6 = 0;
							int value7 = 0;
							int value8 = 0;
							//get 3 unique random numbers between 0 and the size of the breakfast list
							while ((value6 == value7) || (value6 == value8)
									|| (value7 == value8)) {
								value6 = (int) (Math.random()
										* (numberOf1Weight - 0) + 0);
								value7 = (int) (Math.random()
										* (numberOf1Weight - 0) + 0);
								value8 = (int) (Math.random()
										* (numberOf1Weight - 0) + 0);
							}
							//set the food and drink variables to something in those lists
							food1 = foodsW1.get(value6);
							food2 = foodsW1.get(value7);
							food3 = foodsW1.get(value8);
							int numberOfBev = beverages.size() - 1;
							int value9 = (int) (Math.random()
									* (numberOfBev - 0) + 0);
							drink = beverages.get(value9);
							String temp = "\n To eat: \n 1.): " + food1
									+ " \n 2.): " + food2 + " \n 3.): " + food3
									+ " \n and to drink: " + drink + "\n";
							//set the final list
							finalList.add(temp);
							
						} else if (value == 3) {// medium weight food
							int numberOf2Weight = foodsW2.size() - 1;
							int numberOf1Weight = foodsW1.size() - 1;
							int value10 = (int) (Math.random()
									* (numberOf2Weight - 0) + 0);
							int value11 = (int) (Math.random()
									* (numberOf1Weight - 0) + 0);
							int numberOfBev = beverages.size() - 1;
							int value12 = (int) (Math.random()
									* (numberOfBev - 0) + 0);
							food1 = foodsW2.get(value10);
							food2 = foodsW1.get(value11);
							drink = beverages.get(value12);
							String temp = "\n To eat: \n 1.): " + food1
									+ " \n 2.): " + food2
									+ " \n and to drink: " + drink + "\n";
							finalList.add(temp);
							
						} else {//fat food
							if (choices[3] == true || choices[1] == true || choices[9] == true
									|| choices[10] == true) {
								int numberOf1Weight = foodsW1.size() - 1;
								int value6 = 0;
								int value7 = 0;
								int value8 = 0;
								while ((value6 == value7) || (value6 == value8)
										|| (value7 == value8)) {
									value6 = (int) (Math.random()
											* (numberOf1Weight - 0) + 0);
									value7 = (int) (Math.random()
											* (numberOf1Weight - 0) + 0);
									value8 = (int) (Math.random()
											* (numberOf1Weight - 0) + 0);
								}
								food1 = foodsW1.get(value6);
								food2 = foodsW1.get(value7);
								food3 = foodsW1.get(value8);
								int numberOfBev = beverages.size() - 1;
								int value9 = (int) (Math.random()
										* (numberOfBev - 0) + 0);
								drink = beverages.get(value9);
								String temp = "\n To eat: \n 1.): " + food1
										+ " \n 2.): " + food2 + " \n 3.): "
										+ food3 + " \n and to drink: " + drink
										+ "\n";
								finalList.add(temp);
							} else {
								int numberOf3Weight = foodsW3.size() - 1;
								int numberOfBev = beverages.size() - 1;
								int value13 = (int) (Math.random()
										* (numberOf3Weight - 0) + 0);
								int value14 = (int) (Math.random()
										* (numberOfBev - 0) + 0);
								food1 = foodsW3.get(value13);
								drink = beverages.get(value14);
								String temp = "\n To eat: \n" + food1
										+ " \n and to drink: " + drink + "\n";
								finalList.add(temp);
							}
							
						}

					}

					//if monday was checked, display monday meals
					if (days[0] == true) {

						txtMsg.append("****************MONDAY****************\n\n");
						for (int i = 0; i < amount; i++) {
							int random1 = finalList.size() - 1;
							int random2 = (int) (Math.random() * (random1 - 0) + 0);
							txtMsg.append("Meal " + (i + 1) + ": \n"
									+ finalList.get(random2) + "\n");
							finalList.remove(random2);
						}

					}
					//if tuesday was checked, display tuesday meals 
					if (days[1] == true) {
						txtMsg.append("****************TUESDAY****************\n\n");
						for (int i = 0; i < amount; i++) {
							int random1 = finalList.size() - 1;
							int random2 = (int) (Math.random() * (random1 - 0) + 0);
							txtMsg.append("Meal " + (i + 1)
									+ ": \n" + finalList.get(random2) + "\n");
							finalList.remove(random2);
						}
					}
					//if wednesday was checked, display wednesday meals
					if (days[2] == true) {
						txtMsg.append("****************WEDNESDAY****************\n\n");
						for (int i = 0; i < amount; i++) {
							int random1 = finalList.size() - 1;
							int random2 = (int) (Math.random() * (random1 - 0) + 0);
							txtMsg.append("Meal " + (i + 1)
									+ ": \n" + finalList.get(random2) + "\n");
							finalList.remove(random2);
						}
					}
					//if thursday was checked, display thursday meals
					if (days[3] == true) {
						txtMsg.append("****************THURSDAY****************\n\n");
						for (int i = 0; i < amount; i++) {
							int random1 = finalList.size() - 1;
							int random2 = (int) (Math.random() * (random1 - 0) + 0);
							txtMsg.append("Meal " + (i + 1)
									+ ": \n" + finalList.get(random2) + "\n");
							finalList.remove(random2);
						}
					}
					//if friday was checked, display friday meals
					if (days[4] == true) {
						txtMsg.append("****************FRIDAY****************\n\n");
						for (int i = 0; i < amount; i++) {
							int random1 = finalList.size() - 1;
							int random2 = (int) (Math.random() * (random1 - 0) + 0);
							txtMsg.append("Meal " + (i + 1)
									+ ": \n" + finalList.get(random2) + "\n");
							finalList.remove(random2);
						}
					}
					//if saturday was checked, display saturday meals
					if (days[5] == true) {
						txtMsg.append("****************SATURDAY****************\n\n");
						for (int i = 0; i < amount; i++) {
							int random1 = finalList.size() - 1;
							int random2 = (int) (Math.random() * (random1 - 0) + 0);
							txtMsg.append("Meal " + (i + 1)
									+ ": \n" + finalList.get(random2) + "\n");
							finalList.remove(random2);
						}
					}
					//if sunday was checked, display sunday meals 
					if (days[6] == true) {
						txtMsg.append("****************SUNDAY****************\n\n");
						for (int i = 0; i < amount; i++) {
							int random1 = finalList.size() - 1;
							int random2 = (int) (Math.random() * (random1 - 0) + 0);
							txtMsg.append("Meal " + (i + 1)
									+ ": \n" + finalList.get(random2) + "\n");
							finalList.remove(random2);
						}
						txtMsg.append("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
					}

				} catch (Exception e) {
					Toast.makeText(getApplicationContext(), e.getMessage(), 1)
							.show();
				}
				//close database
				db.close();
				

			}
		});

	}

}
