package csu.Bansal;

import csu.Bansal.Options2;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

public class DietPlanner extends ListActivity {
	
	//two buttons, one for full version (not yet coded), one for lite version
	Button btnFull;
	Button btnLite;
	
	//string array for choices to click (atm all lead to same intent)
	String[] items = {"Start new schedule (Full version)", "Start new schedule (Lite version)", "View current schedule"};

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        //set the adapter to our string array
        setListAdapter(new ArrayAdapter<String>(
        		this, android.R.layout.simple_list_item_1,
        		android.R.id.text1, 
        		items));           
    }
    

    protected void onListItemClick(ListView l, View v, int position, long id){
		super.onListItemClick(l, v, position, id);
		
		//on click, send it over to the main gui
		Intent intent;
		intent = new Intent(this, Options2.class);
		Bundle myData = new Bundle();
		myData.putStringArray("items", items);
		myData.putInt("position", position);
		intent.putExtras(myData); 
		startActivity(intent);
			
	}
   
}
